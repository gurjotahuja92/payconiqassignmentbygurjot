RestAssured Automation Framework for Payconiq 

Libraries used :
1. junit
2. testng
3. slf4j-api
4. json-simple
5. rest-assured
6. json-schema-validator
7. lombok

Language - Java 1.8

Build tools - Maven 3.8.1

IDE - IntelliJ 2021.3 (Ultimate Edition)

Installation and Execution details - 
1. Clone this repo into local from https://gitlab.com/gurjotahuja92/payconiqassignmentbygurjot.git
2. Import as a maven project in any IDE and download all the dependencies. 
3. Run mvn clean install or mvn clean compile
4. Go to testRunners file -> Run RestBookerAPITest.xml file to execute all testcases in one go. 
